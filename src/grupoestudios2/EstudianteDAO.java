/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupoestudios2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstudianteDAO {

    String url = "jdbc:mysql://localhost:3306/sma_grupo";
    String user = "root";
    String password = "";

    public List<Estudiante> obtenerEstudiantes() {
        List<Estudiante> estudiantes = new ArrayList<>();
        String query = "SELECT nombres, apellidos, estilo_aprendizaje, carrera, ciclo FROM estudiantes";

        try (Connection connection = DriverManager.getConnection(url, user, password); PreparedStatement statement = connection.prepareStatement(query); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                String nombre = resultSet.getString("nombres");
                String apellido = resultSet.getString("apellidos");
                String estiloAprendizaje = resultSet.getString("estilo_aprendizaje");
                String carrera = resultSet.getString("carrera");
                String ciclo = resultSet.getString("ciclo");
                Estudiante estudiante = new Estudiante(nombre, apellido, estiloAprendizaje, carrera, ciclo);
                estudiantes.add(estudiante);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return estudiantes;
    }

    public List<Estudiante> obtenerEstudiantesPorEstilo(String estilo) {
        List<Estudiante> estudiantes = new ArrayList<>();
        String query = "SELECT nombres, apellidos, estilo_aprendizaje, carrera, ciclo FROM estudiantes WHERE estilo_aprendizaje = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password); PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, estilo);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String nombre = resultSet.getString("nombres");
                    String apellido = resultSet.getString("apellidos");
                    String estiloAprendizaje = resultSet.getString("estilo_aprendizaje");
                    String carrera = resultSet.getString("carrera");
                    String ciclo = resultSet.getString("ciclo");
                    Estudiante estudiante = new Estudiante(nombre, apellido, estiloAprendizaje, carrera, ciclo);
                    estudiantes.add(estudiante);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return estudiantes;
    }

}
