package grupoestudios2;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.List;
import java.util.Random;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//AGENTE PARA FORMAR GRUPOS DE FORMA ALEATORIA

public class AgenteFormacionGrupos extends Agent {

    protected void setup() {
        addBehaviour(new FormacionGruposBehaviour());
    }

    private class FormacionGruposBehaviour extends jade.core.behaviours.CyclicBehaviour {

        public void action() {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
            if (msg != null) {
                String contenido = msg.getContent();
                if (contenido.startsWith("Formar grupo con estilo:")) {
                    String estilo = contenido.split(":")[1];
                    String usuarioLogeado = contenido.split(":")[2]; // Agregar usuario logeado
                    List<Estudiante> estudiantes = obtenerEstudiantesPorEstilo(estilo);
                    Estudiante usuarioLogeadoObjeto = obtenerEstudiantePorNombre(estudiantes, usuarioLogeado);
                    estudiantes.remove(usuarioLogeadoObjeto); // Remove logged-in user
                    Estudiante companero = seleccionarCompanero(estudiantes, usuarioLogeadoObjeto); // Seleccionar compañero aleatorio
                    List<Estudiante> grupo = new ArrayList<>();
                    grupo.add(usuarioLogeadoObjeto);
                    grupo.add(companero);
                    ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                    respuesta.addReceiver(msg.getSender());
                    respuesta.setContent(grupoToString(grupo));
                    send(respuesta);

                    // Mostrar los estudiantes en una GUI
                    mostrarEstudiantes(estudiantes, usuarioLogeadoObjeto);
                }
            } else {
                block();
            }
        }

        private Estudiante seleccionarCompanero(List<Estudiante> estudiantes, Estudiante usuarioLogeado) {
            Random random = new Random();
            int index = random.nextInt(Math.min(estudiantes.size(), 5)); // Limitar la selección a 5 estudiantes como máximo
            return estudiantes.get(index);
        }

    }

    public List<Estudiante> obtenerEstudiantesPorEstilo(String estilo) {
        List<Estudiante> estudiantes = new ArrayList<>();
        String url = "jdbc:mysql://localhost:3306/sma_grupo";
        String user = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            String query = "SELECT nombres, apellidos, estilo_aprendizaje, carrera, ciclo FROM estudiantes WHERE estilo_aprendizaje = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, estilo);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String nombre = resultSet.getString("nombres");
                        String apellido = resultSet.getString("apellidos");
                        String estiloAprendizaje = resultSet.getString("estilo_aprendizaje");
                        String carrera = resultSet.getString("carrera");
                        String ciclo = resultSet.getString("ciclo");
                        estudiantes.add(new Estudiante(nombre, apellido, estiloAprendizaje, carrera, ciclo));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estudiantes;
    }

    private Estudiante obtenerEstudiantePorNombre(List<Estudiante> estudiantes, String nombre) {
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getNombre().equals(nombre)) {
                return estudiante;
            }
        }
        return null;
    }

    private String grupoToString(List<Estudiante> grupo) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Estudiante estudiante : grupo) {
            stringBuilder.append(estudiante.getNombre()).append(",");
        }
        return stringBuilder.toString();
    }

    // Método para mostrar estudiantes en una GUI, si es necesario
    private void mostrarEstudiantes(List<Estudiante> estudiantes, Estudiante usuarioLogeado) {
        Random random = new Random();
        StringBuilder estudiantesString = new StringBuilder();

        // Asegurarse de que el usuario logueado esté incluido primero
        estudiantesString.append(usuarioLogeado.getNombre()).append(" ")
                         .append(usuarioLogeado.getApellido()).append(" ")
                         .append(usuarioLogeado.getEstiloAprendizaje()).append(" ")
                         .append(usuarioLogeado.getCarrera()).append(" ")
                         .append(usuarioLogeado.getCiclo()).append("\n");
        estudiantes.remove(usuarioLogeado); // Eliminar el usuario logeado de la lista

        // Seleccionar aleatoriamente hasta 4 estudiantes adicionales
        for (int i = 0; i < Math.min(estudiantes.size(), 4); i++) {
            int index = random.nextInt(estudiantes.size());
            Estudiante estudiante = estudiantes.get(index);
            estudiantesString.append(estudiante.getNombre()).append(" ")
                             .append(estudiante.getApellido()).append(" ")
                             .append(estudiante.getEstiloAprendizaje()).append(" ")
                             .append(estudiante.getCarrera()).append(" ")
                             .append(estudiante.getCiclo()).append("\n");
            estudiantes.remove(index); // Eliminar el estudiante seleccionado para evitar repetición
        }

        JOptionPane.showMessageDialog(null, estudiantesString.toString(), "Estudiantes por estilo", JOptionPane.INFORMATION_MESSAGE);
    }
}
