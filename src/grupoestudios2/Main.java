package grupoestudios2;

import grupoestudios2.AgenteAutenticacion;
import grupoestudios2.AgenteLogin;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;

public class Main {

    public static void main(String[] args) {
        // Inicializar JADE runtime
        Runtime runtime = Runtime.instance();
        Profile profile = new ProfileImpl();
        profile.setParameter(Profile.MAIN_HOST, "localhost");
        profile.setParameter(Profile.GUI, "true");

        // Crear el contenedor principal
        AgentContainer mainContainer = runtime.createMainContainer(profile);

        try {
            // Crear AgenteAutenticacion
            AgentController agenteAutenticacion = mainContainer.createNewAgent("AgenteAutenticacion", AgenteAutenticacion.class.getName(), null);
            agenteAutenticacion.start();

            // Crear AgenteLogin
            AgentController agenteLogin = mainContainer.createNewAgent("AgenteLogin", AgenteLogin.class.getName(), null);
            agenteLogin.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
