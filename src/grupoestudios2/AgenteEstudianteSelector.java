//Agente que el usuario selecciona sus compañeros de grupo

package grupoestudios2;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AgenteEstudianteSelector extends Agent {

    private List<Estudiante> estudiantes;
    private List<GrupoEstudio> grupos;

    public AgenteEstudianteSelector(List<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
        this.grupos = new ArrayList<>();
    }

    protected void setup() {
        addBehaviour(new EstudianteSelectorBehaviour());
    }

    private class EstudianteSelectorBehaviour extends jade.core.behaviours.CyclicBehaviour {

        public void action() {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
            if (msg != null) {
                String contenido = msg.getContent();
                if (contenido.startsWith("Seleccionar compañeros de grupo con estilo:")) {
                    String estilo = contenido.split(":")[1];
                    List<Estudiante> estudiantesPorEstilo = obtenerEstudiantesPorEstilo(estilo);
                    enviarListaEstudiantes(estudiantesPorEstilo, msg.getSender());
                } else if (contenido.startsWith("Formar grupo:")) {
                    List<Estudiante> seleccionados = new ArrayList<>();
                    String[] seleccionadosIndices = contenido.split(":")[1].split(",");
                    for (String indice : seleccionadosIndices) {
                        int index = Integer.parseInt(indice.trim()) - 1;
                        seleccionados.add(estudiantes.get(index));
                    }
                    if (seleccionados.size() <= 5) {
                        GrupoEstudio grupo = new GrupoEstudio(seleccionados);
                        grupos.add(grupo);
                        ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                        respuesta.addReceiver(msg.getSender());
                        StringBuilder contenidoRespuesta = new StringBuilder("Los siguientes estudiantes han sido seleccionados para el grupo de estudio:\n");
                        for (Estudiante estudiante : seleccionados) {
                            contenidoRespuesta.append("- ").append(estudiante.getNombre()).append(" ").append(estudiante.getApellido())
                                    .append(" (Estilo: ").append(estudiante.getEstiloAprendizaje()).append(", Carrera: ").append(estudiante.getCarrera())
                                    .append(", Ciclo: ").append(estudiante.getCiclo()).append(")\n");
                        }
                        contenidoRespuesta.append("\nChat del grupo:\n");
                        for (String mensaje : grupo.getMensajes()) {
                            contenidoRespuesta.append(mensaje).append("\n");
                        }
                        respuesta.setContent(contenidoRespuesta.toString());
                        send(respuesta);
                    } else {
                        ACLMessage respuestaError = new ACLMessage(ACLMessage.FAILURE);
                        respuestaError.addReceiver(msg.getSender());
                        respuestaError.setContent("No se pueden seleccionar más de 5 estudiantes para el grupo de estudio.");
                        send(respuestaError);
                    }
                } else if (contenido.startsWith("Agregar al grupo:")) {
                    String[] tokens = contenido.split(":");
                    int indiceGrupo = Integer.parseInt(tokens[1].trim()) - 1;
                    GrupoEstudio grupo = grupos.get(indiceGrupo);
                    int indiceEstudiante = Integer.parseInt(tokens[2].trim()) - 1;
                    Estudiante estudiante = estudiantes.get(indiceEstudiante);
                    if (grupo.getEstudiantes().size() < 5) {
                        grupo.agregarEstudiante(estudiante);
                        ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                        respuesta.addReceiver(msg.getSender());
                        respuesta.setContent("Estudiante agregado exitosamente al grupo.");
                        send(respuesta);
                    } else {
                        ACLMessage respuestaError = new ACLMessage(ACLMessage.FAILURE);
                        respuestaError.addReceiver(msg.getSender());
                        respuestaError.setContent("No se pueden agregar más estudiantes al grupo. El grupo ya está completo.");
                        send(respuestaError);
                    }
                }
            } else {
                block();
            }
        }

        private List<Estudiante> obtenerEstudiantesPorEstilo(String estilo) {
            // Convertir el estilo de aprendizaje a un valor numérico
            int estiloNumerico = convertirEstiloANumerico(estilo);

            // Calcular la distancia euclidiana entre el estilo de aprendizaje del estudiante y el estilo buscado
            return estudiantes.stream()
                    .filter(estudiante -> calcularDistanciaEuclidiana(convertirEstiloANumerico(estudiante.getEstiloAprendizaje()), estiloNumerico) == 0)
                    .collect(Collectors.toList());
        }

        private double calcularDistanciaEuclidiana(int estiloEstudiante, int estiloBuscado) {
            return Math.abs(estiloEstudiante - estiloBuscado);
        }

        private int convertirEstiloANumerico(String estilo) {
            // Aquí puedes definir tu propia lógica para asignar un valor numérico a cada estilo de aprendizaje
            // Por ejemplo, podrías asignar valores del 1 al n dependiendo de la cantidad de estilos que tengas
            if (estilo.equalsIgnoreCase("visual")) {
                return 1;
            } else if (estilo.equalsIgnoreCase("auditivo")) {
                return 2;
            } else if (estilo.equalsIgnoreCase("kinestésico")) {
                return 3;
            } else {
                // Por defecto, devolver 0 para estilos desconocidos
                return 0;
            }
        }

        private void enviarListaEstudiantes(List<Estudiante> estudiantes, AID receiver) {
            ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
            respuesta.addReceiver(receiver);
            StringBuilder contenido = new StringBuilder();
            contenido.append("Seleccione sus compañeros de grupo:\n");
            for (int i = 0; i < estudiantes.size(); i++) {
                contenido.append(i + 1).append(". ").append(estudiantes.get(i).getNombre()).append(" ")
                        .append(estudiantes.get(i).getApellido()).append(" (Estilo: ").append(estudiantes.get(i).getEstiloAprendizaje())
                        .append(", Carrera: ").append(estudiantes.get(i).getCarrera()).append(", Ciclo: ")
                        .append(estudiantes.get(i).getCiclo()).append(")\n");
            }
            respuesta.setContent(contenido.toString());
            send(respuesta);
        }
    }
}
