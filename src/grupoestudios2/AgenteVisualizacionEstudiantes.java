/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupoestudios2;

/**
 *
 * @author santiagotene
 */
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AgenteVisualizacionEstudiantes extends Agent {

    protected void setup() {
        addBehaviour(new EstudiantesPorEstiloBehaviour());
    }

    private class EstudiantesPorEstiloBehaviour extends jade.core.behaviours.CyclicBehaviour {

        public void action() {
            System.out.println("****PASA POR ACTION AGENTE VISUALIZAR");
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
            if (msg != null) {
                String contenido = msg.getContent();
                if (contenido.startsWith("Estudiantes por estilo:")) {
                    String estilo = contenido.split(":")[1];
                    List<String> estudiantes = obtenerEstudiantesPorEstilo(estilo);
                    ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                    respuesta.addReceiver(msg.getSender());
                    respuesta.setContent(String.join(",", estudiantes));
                    send(respuesta);
                }
            } else {
                block();
            }
        }
    }

    public List<String> obtenerEstudiantesPorEstilo(String estilo) {

        System.out.println("****PRUEBA ESTILO");
        List<String> estudiantes = new ArrayList<>();
        String url = "jdbc:mysql://localhost:3306/sma_grupo";
        String user = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            String query = "SELECT nombres, apellidos, estilo_aprendizaje, carrera, ciclo FROM estudiantes WHERE estilo_aprendizaje = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, estilo);
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        String nombre = resultSet.getString("nombres");
                        String apellido = resultSet.getString("apellidos");
                        String estilos = resultSet.getString("estilo_aprendizaje");
                        String carrera = resultSet.getString("carrera");
                        String ciclo = resultSet.getString("ciclo");
                        estudiantes.add(nombre + " " + apellido + " " + estilos + " " + carrera + " " + ciclo);
                        System.out.println("query" + estilos);                        
                    }
                    VisualizacionEstudiantesGui gui = new VisualizacionEstudiantesGui();
                    gui.mostrarEstudiantes(estudiantes);
                    gui.setVisible(true);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estudiantes;
    }
}
