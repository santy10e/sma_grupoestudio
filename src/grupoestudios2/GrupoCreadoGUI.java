package grupoestudios2;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class GrupoCreadoGUI extends JFrame {

    private JTextArea chatTextArea;
    private JTextField mensajeTextField;
    private JButton enviarButton;
    private JButton salirButton;

    public GrupoCreadoGUI(List<Estudiante> estudiantesSeleccionados) {
        setTitle("Grupo Creado");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel mainPanel = new JPanel(new BorderLayout());

        // Crear tabla para mostrar los estudiantes seleccionados
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Estilo Aprendizaje");
        model.addColumn("Carrera");
        model.addColumn("Ciclo");

        for (Estudiante estudiante : estudiantesSeleccionados) {
            model.addRow(new Object[]{estudiante.getNombre(), estudiante.getApellido(), estudiante.getEstiloAprendizaje(), estudiante.getCarrera(), estudiante.getCiclo()});
        }

        JTable estudiantesTable = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(estudiantesTable);
        mainPanel.add(scrollPane, BorderLayout.CENTER);

        // Panel para el chat
        JPanel chatPanel = new JPanel(new BorderLayout());
        chatTextArea = new JTextArea();
        chatTextArea.setEditable(false);
        JScrollPane chatScrollPane = new JScrollPane(chatTextArea);
        chatScrollPane.setPreferredSize(new Dimension(300, 400)); // Ajusta el tamaño preferido del JScrollPane
        chatPanel.add(chatScrollPane, BorderLayout.CENTER);
        mainPanel.add(chatPanel, BorderLayout.EAST);

// Campo de texto para escribir mensajes
        JPanel mensajePanel = new JPanel(new BorderLayout());
        mensajeTextField = new JTextField();
        mensajeTextField.setPreferredSize(new Dimension(300, 50)); // Ajusta el tamaño preferido del JTextField
        mensajePanel.add(mensajeTextField, BorderLayout.CENTER);
        mainPanel.add(mensajePanel, BorderLayout.SOUTH); // Agrega mensajePanel al sur del mainPanel

        // Botón para enviar mensaje
        enviarButton = new JButton("Enviar");
        enviarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mensaje = mensajeTextField.getText();
                if (!mensaje.isEmpty()) {
                    agregarMensajeChat(mensaje);
                    mensajeTextField.setText(""); // Limpiar campo de texto después de enviar mensaje
                }
            }
        });
        mensajePanel.add(enviarButton, BorderLayout.EAST);
        chatPanel.add(mensajePanel, BorderLayout.SOUTH);

        // Botón para salir del grupo
        salirButton = new JButton("Salir del grupo");
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Cerrar la ventana
            }
        });
        mainPanel.add(salirButton, BorderLayout.SOUTH);

        getContentPane().add(mainPanel);
    }

    public void agregarMensajeChat(String mensaje) {
        chatTextArea.append(mensaje + "\n");
    }
}
