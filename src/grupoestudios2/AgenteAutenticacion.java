package grupoestudios2;


import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.sql.*;

public class AgenteAutenticacion extends Agent {

    protected void setup() {
        addBehaviour(new AutenticacionBehaviour());
    }

    private class AutenticacionBehaviour extends jade.core.behaviours.CyclicBehaviour {

        public void action() {
            ACLMessage msg = receive();
            if (msg != null) {
                String contenido = msg.getContent();
                if (contenido.startsWith("Inicio de sesion:")) {
                    String[] credenciales = contenido.split(":");
                    String usuario = credenciales[1];
                    String contrasena = credenciales[2];
                    boolean autenticado = autenticarUsuario(usuario, contrasena);
                    ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                    respuesta.addReceiver(msg.getSender());
                    respuesta.setContent(Boolean.toString(autenticado));
                    send(respuesta);
                } else if (contenido.startsWith("Obtener datos estudiante:")) {
                    String usuario = contenido.split(":")[1];
                    String[] datosEstudiante = obtenerDatosEstudiante(usuario);
                    ACLMessage respuesta = new ACLMessage(ACLMessage.INFORM);
                    respuesta.addReceiver(msg.getSender());
                    respuesta.setContent(String.join(":", datosEstudiante));
                    send(respuesta);
                }
            } else {
                block();
            }
        }
    }

    private boolean autenticarUsuario(String usuario, String contrasena) {
        String url = "jdbc:mysql://localhost:3306/sma_grupo";
        String user = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            String query = "SELECT * FROM estudiantes WHERE usuario = ? AND contraseña = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, usuario);
                statement.setString(2, contrasena);
                try (ResultSet resultSet = statement.executeQuery()) {
                    return resultSet.next();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private String[] obtenerDatosEstudiante(String usuario) {
        String[] datosEstudiante = new String[3]; // Nombre, Apellido, Estilo de Aprendizaje
        String url = "jdbc:mysql://localhost:3306/sma_grupo";
        String user = "root";
        String password = "";

        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            String query = "SELECT nombres, apellidos, estilo_aprendizaje FROM estudiantes WHERE usuario = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, usuario);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        datosEstudiante[0] = resultSet.getString("nombres");
                        datosEstudiante[1] = resultSet.getString("apellidos");
                        datosEstudiante[2] = resultSet.getString("estilo_aprendizaje");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return datosEstudiante;
    }
}
