package grupoestudios2;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class VisualizacionEstudiantesGui extends JFrame {

    private JTable estudiantesTable;

    public VisualizacionEstudiantesGui() {
        super("Visualización de Estudiantes");

        // Configurar la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        setLocationRelativeTo(null);

        // Crear panel principal
        JPanel panel = new JPanel(new BorderLayout());

        // Panel para la tabla de estudiantes
        JPanel tablaPanel = new JPanel(new BorderLayout());
        estudiantesTable = new JTable();
        JScrollPane scrollPane = new JScrollPane(estudiantesTable);
        tablaPanel.add(scrollPane, BorderLayout.CENTER);

        // Panel para el botón de cerrar
        JPanel cerrarPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton cerrarButton = new JButton("Cerrar");
        cerrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Cierra la ventana
            }
        });
        cerrarPanel.add(cerrarButton);

        // Agregar paneles al panel principal
        panel.add(tablaPanel, BorderLayout.CENTER);
        panel.add(cerrarPanel, BorderLayout.SOUTH);

        // Agregar panel a la ventana
        getContentPane().add(panel);
    }

    public void mostrarEstudiantes(List<String> estudiantes) {
        // Mostrar datos en la tabla
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Estilo Aprendizaje");
        model.addColumn("Carrera");
        model.addColumn("Ciclo");

        for (String estudiante : estudiantes) {
            String[] partes = estudiante.split(" ");
            if (partes.length >= 5) {
                model.addRow(new Object[]{partes[0], partes[1], partes[2], partes[3], partes[4]});
            }
        }

        estudiantesTable.setModel(model);
    }
}
