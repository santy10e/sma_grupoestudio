package grupoestudios2;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class GrupoEstudioGUI extends JFrame {

    private JTable estudiantesTable;
    private JButton agregarParticipanteButton;
    private JButton crearGrupoButton;
    private JButton limpiarSeleccionButton;
    private List<Estudiante> estudiantesSeleccionados;
    private DefaultTableModel estudiantesSeleccionadosModel;

    public GrupoEstudioGUI(List<Estudiante> estudiantes) {
        setTitle("Seleccionar compañeros de grupo");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        estudiantesSeleccionados = new ArrayList<>();

        JPanel mainPanel = new JPanel(new BorderLayout());

        JPanel tablePanel = new JPanel(new BorderLayout());
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Estilo Aprendizaje");
        model.addColumn("Carrera");
        model.addColumn("Ciclo");
        agregarFilas(model, estudiantes);
        estudiantesTable = new JTable(model);
        tablePanel.add(new JScrollPane(estudiantesTable), BorderLayout.CENTER);
        mainPanel.add(tablePanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        agregarParticipanteButton = new JButton("Agregar Participante");
        crearGrupoButton = new JButton("Crear Grupo");
        limpiarSeleccionButton = new JButton("Limpiar Selección");

        agregarParticipanteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int filaSeleccionada = estudiantesTable.getSelectedRow();
                if (filaSeleccionada != -1) {
                    String nombre = (String) estudiantesTable.getValueAt(filaSeleccionada, 0);
                    String apellido = (String) estudiantesTable.getValueAt(filaSeleccionada, 1);
                    String estilo = (String) estudiantesTable.getValueAt(filaSeleccionada, 2);
                    String carrera = (String) estudiantesTable.getValueAt(filaSeleccionada, 3);
                    String ciclo = (String) estudiantesTable.getValueAt(filaSeleccionada, 4);
                    Estudiante estudiante = new Estudiante(nombre, apellido, estilo, carrera, ciclo);
                    estudiantesSeleccionados.add(estudiante);
                    estudiantesSeleccionadosModel.addRow(new Object[]{nombre, apellido, estilo, carrera, ciclo});
                } else {
                    JOptionPane.showMessageDialog(GrupoEstudioGUI.this, "Seleccione un estudiante para agregar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        crearGrupoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (estudiantesSeleccionados.size() == 5) {
                    // Crear la ventana GrupoCreadoGUI
                    GrupoCreadoGUI grupoCreadoGUI = new GrupoCreadoGUI(estudiantesSeleccionados);
                    // Establecer la visibilidad de la ventana como verdadera para mostrarla
                    grupoCreadoGUI.setVisible(true);
                    // Cerrar la ventana actual
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(GrupoEstudioGUI.this, "Debe seleccionar 5 estudiantes para crear el grupo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        limpiarSeleccionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                estudiantesSeleccionados.clear();
                estudiantesSeleccionadosModel.setRowCount(0);
            }
        });

        buttonPanel.add(agregarParticipanteButton);
        buttonPanel.add(crearGrupoButton);
        buttonPanel.add(limpiarSeleccionButton);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        JPanel estudiantesSeleccionadosPanel = new JPanel(new BorderLayout());
        estudiantesSeleccionadosModel = new DefaultTableModel();
        estudiantesSeleccionadosModel.addColumn("Nombre");
        estudiantesSeleccionadosModel.addColumn("Apellido");
        estudiantesSeleccionadosModel.addColumn("Estilo Aprendizaje");
        estudiantesSeleccionadosModel.addColumn("Carrera");
        estudiantesSeleccionadosModel.addColumn("Ciclo");
        JTable estudiantesSeleccionadosTable = new JTable(estudiantesSeleccionadosModel);
        estudiantesSeleccionadosPanel.add(new JScrollPane(estudiantesSeleccionadosTable), BorderLayout.CENTER);
        mainPanel.add(estudiantesSeleccionadosPanel, BorderLayout.EAST);

        getContentPane().add(mainPanel);
    }

    private void agregarFilas(DefaultTableModel model, List<Estudiante> estudiantes) {
        for (Estudiante estudiante : estudiantes) {
            model.addRow(new Object[]{estudiante.getNombre(), estudiante.getApellido(), estudiante.getEstiloAprendizaje(), estudiante.getCarrera(), estudiante.getCiclo()});
        }
    }

    public static void main(String[] args) {
        EstudianteDAO estudianteDAO = new EstudianteDAO();
        List<Estudiante> estudiantes = estudianteDAO.obtenerEstudiantes();
        if (!estudiantes.isEmpty()) {
            GrupoEstudioGUI gui = new GrupoEstudioGUI(estudiantes);
            gui.setVisible(true);
        } else {
            System.out.println("No se encontraron estudiantes en la base de datos.");
        }
    }
}
