package grupoestudios2;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AgenteLogin extends Agent {

    private JTextField usuarioField;
    private JPasswordField contrasenaField;

    protected void setup() {
        // Crear interfaz gráfica
        SwingUtilities.invokeLater(this::crearInterfazGrafica);
    }

    private void crearInterfazGrafica() {
        JFrame frame = new JFrame("Iniciar Sesión");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300); // Tamaño de la ventana
        frame.setLocationRelativeTo(null); // Centrar en la pantalla

        // Panel principal con BorderLayout para colocar la imagen en la parte superior
        JPanel panel = new JPanel(new BorderLayout());

        // Panel para el formulario con GridBagLayout para un diseño más flexible
        JPanel formPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10); // Espacios entre componentes

        // Cargar y mostrar la imagen en la parte superior
        ImageIcon imagenIcon = new ImageIcon("/src/img/login.png"); // Cambia la ruta por la ruta de tu imagen
        JLabel imagenLabel = new JLabel(imagenIcon);
        panel.add(imagenLabel, BorderLayout.NORTH);

        // Etiquetas y campos de texto del formulario
        JLabel usuarioLabel = new JLabel("Usuario:");
        gbc.gridx = 0;
        gbc.gridy = 0;
        formPanel.add(usuarioLabel, gbc);

        usuarioField = new JTextField(15); // Campo de texto más ancho
        gbc.gridx = 1;
        gbc.gridy = 0;
        formPanel.add(usuarioField, gbc);

        JLabel contrasenaLabel = new JLabel("Contraseña:");
        gbc.gridx = 0;
        gbc.gridy = 1;
        formPanel.add(contrasenaLabel, gbc);

        contrasenaField = new JPasswordField(15); // Campo de texto más ancho
        gbc.gridx = 1;
        gbc.gridy = 1;
        formPanel.add(contrasenaField, gbc);

        // Botones de inicio de sesión y registro
        JButton loginButton = new JButton("Iniciar Sesión");
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2; // Ocupa dos columnas
        gbc.fill = GridBagConstraints.HORIZONTAL; // Estirar horizontalmente
        gbc.anchor = GridBagConstraints.CENTER; // Alinear al centro
        gbc.insets = new Insets(20, 10, 0, 10); // Espacios adicionales en la parte superior
        formPanel.add(loginButton, gbc);

        // Acción del botón de inicio de sesión
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String usuario = usuarioField.getText();
                String contrasena = new String(contrasenaField.getPassword());
                iniciarSesion(usuario, contrasena);
            }
        });

        JButton registroButton = new JButton("Registrarse");
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2; // Ocupa dos columnas
        gbc.fill = GridBagConstraints.HORIZONTAL; // Estirar horizontalmente
        gbc.anchor = GridBagConstraints.CENTER; // Alinear al centro
        gbc.insets = new Insets(10, 10, 10, 10); // Espacios entre botones
        formPanel.add(registroButton, gbc);
        
        //Accion del boton de registrarse
        registroButton.addActionListener(new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent e) {
               RegistroGUI registro = new RegistroGUI();
               registro.setVisible(true);
            }
        });


        // Agregar el panel del formulario al centro del panel principal
        panel.add(formPanel, BorderLayout.CENTER);

        // Agregar el panel principal a la ventana
        frame.getContentPane().add(panel);
        frame.setVisible(true);
    }

    private void iniciarSesion(String usuario, String contrasena) {
        // Crear mensaje para enviar credenciales al AgenteAutenticacion
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(getAID("AgenteAutenticacion"));
        msg.setContent("Inicio de sesion:" + usuario + ":" + contrasena);
        send(msg);

        // Esperar la respuesta del AgenteAutenticacion
        ACLMessage respuesta = blockingReceive();
        if (respuesta != null && respuesta.getContent().equals("true")) {
            JOptionPane.showMessageDialog(null, "Sesión iniciada correctamente para el usuario: " + usuario);

            // Abrir la ventana del estudiante
            abrirVentanaEstudiante(usuario);
            
        } else {
            JOptionPane.showMessageDialog(null, "Error al iniciar sesión para el usuario: " + usuario);
        }
    }

    private void abrirVentanaEstudiante(String usuario) {
        // Crear mensaje para solicitar datos del estudiante al AgenteAutenticacion
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(getAID("AgenteAutenticacion"));
        msg.setContent("Obtener datos estudiante:" + usuario);
        send(msg);

        // Esperar la respuesta del AgenteAutenticacion
        ACLMessage respuesta = blockingReceive();
        if (respuesta != null && respuesta.getPerformative() == ACLMessage.INFORM) {
            String[] datosEstudiante = respuesta.getContent().split(":");
            String nombres = datosEstudiante[0];
            String apellidos = datosEstudiante[1];
            String estiloAprendizaje = datosEstudiante[2];

            // Mostrar los datos del estudiante en la consola
            System.out.println("Nombre: " + nombres);
            System.out.println("Apellidos: " + apellidos);
            System.out.println("Estilo de Aprendizaje: " + estiloAprendizaje);

            // Abrir la ventana de estudiante
            VentanaEstudiante ventanaEstudiante = new VentanaEstudiante(nombres, apellidos, estiloAprendizaje);
            ventanaEstudiante.setVisible(true);
        } else {
            System.out.println("Error al obtener datos del estudiante.");
        }
    }
}
