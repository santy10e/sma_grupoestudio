package grupoestudios2;

import java.util.ArrayList;
import java.util.List;

public class GrupoEstudio {
    private List<Estudiante> estudiantes;
    private List<String> mensajes;

    public GrupoEstudio(List<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
        this.mensajes = new ArrayList<>();
    }

    public List<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public List<String> getMensajes() {
        return mensajes;
    }

    public void agregarEstudiante(Estudiante estudiante) {
        estudiantes.add(estudiante);
    }

    public void agregarMensaje(String mensaje) {
        mensajes.add(mensaje);
    }
}
