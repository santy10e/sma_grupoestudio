/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package grupoestudios2;

/**
 *
 * @author santiagotene
 */
public class Estudiante {
    private String nombre;
    private String apellido;
    private String estiloAprendizaje;
    private String carrera;
    private String ciclo;

    // Constructor
    public Estudiante(String nombre, String apellido, String estiloAprendizaje, String carrera, String ciclo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.estiloAprendizaje = estiloAprendizaje;
        this.carrera = carrera;
        this.ciclo = ciclo;
    }

    // Getters
    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEstiloAprendizaje() {
        return estiloAprendizaje;
    }

    public String getCarrera() {
        return carrera;
    }

    public String getCiclo() {
        return ciclo;
    }
}
