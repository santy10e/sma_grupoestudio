package grupoestudios2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;

public class RegistroGUI extends JFrame implements ActionListener {

    private JButton btnRegistrar;
    private JButton btnIniciarSesion;
    private JTextField txtNombres;
    private JTextField txtApellidos;
    private JTextField txtCarrera;
    private JTextField txtCiclo;
    private JTextField txtCedula;
    private JTextField txtCiudad;
    private JTextField txtUsuario;
    private JPasswordField txtContraseña;
    private JComboBox<String> cmbPregunta1;
    private JComboBox<String> cmbPregunta2;
    private JComboBox<String> cmbPregunta3;
    private JComboBox<String> cmbPregunta4;

    public RegistroGUI() {
        initComponents();
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Registro de Estudiante");

        JPanel panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        JLabel lblNombres = new JLabel("Nombres:");
        txtNombres = new JTextField(20);
        JLabel lblApellidos = new JLabel("Apellidos:");
        txtApellidos = new JTextField(20);
        JLabel lblCarrera = new JLabel("Carrera:");
        txtCarrera = new JTextField(20);
        JLabel lblCiclo = new JLabel("Ciclo:");
        txtCiclo = new JTextField(20);
        JLabel lblCedula = new JLabel("Cedula:");
        txtCedula = new JTextField(20);
        JLabel lblCiudad = new JLabel("Ciudad:");
        txtCiudad = new JTextField(20);

        JLabel lblPregunta1 = new JLabel("¿Cómo prefieres aprender?");
        cmbPregunta1 = new JComboBox<>(new String[]{"Leer", "Escuchar", "Ver videos"});
        JLabel lblPregunta2 = new JLabel("¿Cuál es tu método de estudio preferido?");
        cmbPregunta2 = new JComboBox<>(new String[]{"Lectura", "Discusión en grupo", "Resolución de problemas"});
        JLabel lblPregunta3 = new JLabel("¿Qué actividad te resulta más útil para aprender?");
        cmbPregunta3 = new JComboBox<>(new String[]{"Experimentar", "Observar", "Escuchar"});
        JLabel lblPregunta4 = new JLabel("¿Cómo prefieres practicar lo que has aprendido?");
        cmbPregunta4 = new JComboBox<>(new String[]{"Práctica", "Repaso", "Explicación a otros"});

        btnRegistrar = new JButton("Registrar");
        btnRegistrar.addActionListener(this);

        JLabel lblUsuario = new JLabel("Usuario:");
        txtUsuario = new JTextField(20);
        JLabel lblContraseña = new JLabel("Contraseña:");
        txtContraseña = new JPasswordField(20);

        btnIniciarSesion = new JButton("Iniciar Sesión");
        btnIniciarSesion.addActionListener(this);

        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        hGroup.addGroup(layout.createParallelGroup()
                .addComponent(lblNombres)
                .addComponent(lblApellidos)
                .addComponent(lblCarrera)
                .addComponent(lblCiclo)
                .addComponent(lblCedula)
                .addComponent(lblCiudad)
                .addComponent(lblPregunta1)
                .addComponent(lblPregunta2)
                .addComponent(lblPregunta3)
                .addComponent(lblPregunta4)
                .addComponent(lblUsuario)
                .addComponent(lblContraseña)
                .addComponent(btnRegistrar)
                .addComponent(btnIniciarSesion)
        );
        hGroup.addGroup(layout.createParallelGroup()
                .addComponent(txtNombres)
                .addComponent(txtApellidos)
                .addComponent(txtCarrera)
                .addComponent(txtCiclo)
                .addComponent(txtCedula)
                .addComponent(txtCiudad)
                .addComponent(cmbPregunta1)
                .addComponent(cmbPregunta2)
                .addComponent(cmbPregunta3)
                .addComponent(cmbPregunta4)
                .addComponent(txtUsuario)
                .addComponent(txtContraseña)
        );
        layout.setHorizontalGroup(hGroup);

        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblNombres)
                .addComponent(txtNombres)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblApellidos)
                .addComponent(txtApellidos)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblCarrera)
                .addComponent(txtCarrera)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblCiclo)
                .addComponent(txtCiclo)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblCedula)
                .addComponent(txtCedula)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblCiudad)
                .addComponent(txtCiudad)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblPregunta1)
                .addComponent(cmbPregunta1)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblPregunta2)
                .addComponent(cmbPregunta2)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblPregunta3)
                .addComponent(cmbPregunta3)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblPregunta4)
                .addComponent(cmbPregunta4)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblUsuario)
                .addComponent(txtUsuario)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblContraseña)
                .addComponent(txtContraseña)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(btnRegistrar)
        );
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(btnIniciarSesion)
        );
        layout.setVerticalGroup(vGroup);

        add(panel);

        pack();
        setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnRegistrar) {
            registrarEstudiante();
        } else if (e.getSource() == btnIniciarSesion) {
            iniciarSesion();
        }
    }

    private void registrarEstudiante() {
        String nombres = txtNombres.getText();
        String apellidos = txtApellidos.getText();
        String carrera = txtCarrera.getText();
        String ciclo = txtCiclo.getText();
        String cedula = txtCedula.getText();
        String ciudad = txtCiudad.getText();
        String usuario = txtUsuario.getText();
        String contraseña = new String(txtContraseña.getPassword());
        String respuesta1 = (String) cmbPregunta1.getSelectedItem();
        String respuesta2 = (String) cmbPregunta2.getSelectedItem();
        String respuesta3 = (String) cmbPregunta3.getSelectedItem();
        String respuesta4 = (String) cmbPregunta4.getSelectedItem();

        String estiloAprendizaje = clasificarEstiloAprendizaje(respuesta1, respuesta2, respuesta3, respuesta4);
        
        // Guardar en la base de datos
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sma_grupo", "root", "");
            String query = "INSERT INTO estudiantes (nombres, apellidos, carrera, ciclo, cedula, ciudad, usuario, contraseña, estilo_aprendizaje) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, nombres);
            pst.setString(2, apellidos);
            pst.setString(3, carrera);
            pst.setString(4, ciclo);
            pst.setString(5, cedula);
            pst.setString(6, ciudad);
            pst.setString(7, usuario);
            pst.setString(8, contraseña);
            pst.setString(9, estiloAprendizaje);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(this, "Estudiante registrado exitosamente con estilo de aprendizaje: " + estiloAprendizaje);
            conn.close();
            limpiarCampos();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error al registrar estudiante");
        }
        
    }

    private void iniciarSesion() {
        this.dispose();

        // Crear una nueva instancia de AgenteLoginGui y hacerla visible
        //AgenteLogin loginGui = new AgenteLogin();
        //loginGui.setVisible(true);
    }

    private void limpiarCampos() {
        txtNombres.setText("");
        txtApellidos.setText("");
        txtCarrera.setText("");
        txtCiclo.setText("");
        txtCedula.setText("");
        txtCiudad.setText("");
        txtUsuario.setText("");
        txtContraseña.setText("");
        cmbPregunta1.setSelectedIndex(0);
        cmbPregunta2.setSelectedIndex(0);
        cmbPregunta3.setSelectedIndex(0);
        cmbPregunta4.setSelectedIndex(0);
    }

    private String clasificarEstiloAprendizaje(String respuesta1, String respuesta2, String respuesta3, String respuesta4) {
        // Aquí se implementa un algoritmo simple para clasificar el estilo de aprendizaje
        // Este es solo un ejemplo y puede ser mejorado según tus necesidades y criterios de clasificación

        // Verificamos las respuestas a las preguntas 1 y 2 para determinar el estilo de aprendizaje
        if (respuesta1.equals("Leer") && respuesta2.equals("Lectura")) {
            return "Visual";
        } else if (respuesta1.equals("Escuchar") && respuesta2.equals("Discusión en grupo")) {
            return "Auditivo";
        } else {
            // Si las respuestas a las preguntas 1 y 2 no coinciden con los estilos conocidos, 
            // evaluamos las respuestas a las preguntas 3 y 4 para determinar el estilo de aprendizaje
            if (respuesta3.equals("Experimentar") && respuesta4.equals("Práctica")) {
                return "Kinestésico";
            } else {
                // Si ninguna de las combinaciones de respuestas coincide con los estilos conocidos,
                // podrías agregar más lógica de clasificación aquí o retornar un valor por defecto
                return "Indefinido";
            }
        }
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new RegistroGUI().setVisible(true);
        });
    }
}
