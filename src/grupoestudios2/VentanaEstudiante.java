package grupoestudios2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.util.List;
import grupoestudios2.Estudiante;
import java.util.ArrayList;

public class VentanaEstudiante extends JFrame {

    private JLabel nombreLabel;
    private JLabel apellidoLabel;
    private JLabel estiloAprendizajeLabel;
    private JLabel fotoLabel;

    // Ruta de la imagen de perfil
    private static final String PATH_TO_IMAGE = "ruta/de/la/imagen.jpg";

    public VentanaEstudiante(String nombres, String apellidos, String estiloAprendizaje) {
        super("Ventana Estudiante");

        // Configurar la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 500);
        setLocationRelativeTo(null);
        setResizable(false);

        // Crear panel principal con BorderLayout
        JPanel panel = new JPanel(new BorderLayout());

        // Panel para los datos del estudiante
        JPanel datosPanel = new JPanel(new GridLayout(4, 1, 10, 10));
        datosPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        // Etiquetas para mostrar los datos del estudiante
        nombreLabel = new JLabel("Nombre: " + nombres);
        apellidoLabel = new JLabel("Apellido: " + apellidos);
        estiloAprendizajeLabel = new JLabel("Estilo de Aprendizaje: " + estiloAprendizaje);

        // Cargar la foto de perfil
        fotoLabel = new JLabel();
        try {
            BufferedImage img = ImageIO.read(new File(PATH_TO_IMAGE));
            ImageIcon icon = new ImageIcon(img);
            fotoLabel.setIcon(icon);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Agregar etiquetas al panel de datos
        datosPanel.add(nombreLabel);
        datosPanel.add(apellidoLabel);
        datosPanel.add(estiloAprendizajeLabel);
        datosPanel.add(fotoLabel);

        // Panel para los botones
        JPanel botonesPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));

        // Botones
        JButton verEstudiantesButton = new JButton("Ver Estudiantes");
        JButton generarGruposButton = new JButton("Generar Grupos");
        JButton crearGrupoEstudioButton = new JButton("Crear Grupo de Estudio");

        // Estilo para los botones
        verEstudiantesButton.setPreferredSize(new Dimension(200, 50));
        generarGruposButton.setPreferredSize(new Dimension(200, 50));
        crearGrupoEstudioButton.setPreferredSize(new Dimension(200, 50));

        // Acciones de los botones
        verEstudiantesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AgenteVisualizacionEstudiantes agenteVisualizacionEstudiantes = new AgenteVisualizacionEstudiantes();
                agenteVisualizacionEstudiantes.obtenerEstudiantesPorEstilo(estiloAprendizaje);
            }
        });

        // Boton para generar grupo ALEATORIO
        generarGruposButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Crear un objeto AgenteFormacionGrupos
                AgenteFormacionGrupos agente = new AgenteFormacionGrupos();

                // Obtener estudiantes por estilo
                List<Estudiante> estudiantes = agente.obtenerEstudiantesPorEstilo(estiloAprendizaje);

                // Obtener el estudiante logueado
                Estudiante usuarioLogeado = obtenerUsuarioLogeado(nombres, estudiantes);

                // Mostrar la GUI de grupos con los estudiantes obtenidos
                GruposGUI gruposGUI = new GruposGUI(estudiantes, usuarioLogeado);
                gruposGUI.setVisible(true);
            }
        });

// Botón para crear grupo de estudio
        crearGrupoEstudioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Obtener el usuario logueado
                EstudianteDAO estudianteDAO = new EstudianteDAO();
                List<Estudiante> estudiantes = estudianteDAO.obtenerEstudiantesPorEstilo(estiloAprendizaje);
                if (!estudiantes.isEmpty()) {
                    Estudiante usuarioLogeado = estudiantes.get(0); // Supongamos que el usuario logueado es el primer estudiante de la lista

                    // Crear una instancia de EstudianteSelectorAgent y pasarle la lista de estudiantes
                    AgenteEstudianteSelector estudianteSelector = new AgenteEstudianteSelector(estudiantes);
                    estudianteSelector.setup();

                    // Crear y mostrar GrupoEstudioGUI con el usuario logueado ya seleccionado
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            GrupoEstudioGUI grupoEstudioGUI = new GrupoEstudioGUI(estudiantes);
                            grupoEstudioGUI.setVisible(true);
                        }
                    });
                } else {
                    System.out.println("No se encontraron estudiantes en la base de datos.");
                }
            }
        });

        // Agregar botones al panel de botones
        botonesPanel.add(verEstudiantesButton);
        botonesPanel.add(generarGruposButton);
        botonesPanel.add(crearGrupoEstudioButton);

        // Agregar paneles al panel principal
        panel.add(datosPanel, BorderLayout.CENTER);
        panel.add(botonesPanel, BorderLayout.SOUTH);

        // Agregar panel principal a la ventana
        getContentPane().add(panel);
    }

    private Estudiante obtenerUsuarioLogeado(String nombres, List<Estudiante> estudiantes) {
        for (Estudiante estudiante : estudiantes) {
            if (estudiante.getNombre().equals(nombres)) {
                return estudiante;
            }
        }
        return null;
    }
}
