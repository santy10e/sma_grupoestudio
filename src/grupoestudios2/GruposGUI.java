package grupoestudios2;

// GUI PARA GRUPO ALEATORIO

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;

public class GruposGUI extends JFrame {

    private JTable estudiantesTable;
    private JButton salirButton;
    private JTextArea chatTextArea;
    private JTextField mensajeTextField;
    private JButton enviarButton;

    public GruposGUI(List<Estudiante> estudiantes, Estudiante usuarioLogeado) {
        setTitle("Grupos de Estudiantes prueba");
        setSize(800, 600); // Cambié el tamaño para que se vea mejor
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel(new BorderLayout());

        // Crear modelo de tabla
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Apellido");
        model.addColumn("Estilo Aprendizaje");
        model.addColumn("Carrera");
        model.addColumn("Ciclo");

        // Agregar filas al modelo
        agregarFilas(model, estudiantes, usuarioLogeado);

        // Crear tabla con el modelo
        estudiantesTable = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(estudiantesTable);

        panel.add(scrollPane, BorderLayout.CENTER);

        // Panel para el chat
        JPanel chatPanel = new JPanel(new BorderLayout());
        chatTextArea = new JTextArea();
        chatTextArea.setEditable(false);
        JScrollPane chatScrollPane = new JScrollPane(chatTextArea);
        chatScrollPane.setPreferredSize(new Dimension(200, 400));
        chatPanel.add(chatScrollPane, BorderLayout.CENTER);

        // Campo de texto para escribir mensajes
        JPanel mensajePanel = new JPanel(new BorderLayout());
        mensajeTextField = new JTextField();
        mensajePanel.add(mensajeTextField, BorderLayout.CENTER);

        // Botón para enviar mensaje
        enviarButton = new JButton("Enviar");
        enviarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String mensaje = mensajeTextField.getText();
                if (!mensaje.isEmpty()) {
                    agregarMensajeChat(usuarioLogeado.getNombre() + ": " + mensaje);
                    mensajeTextField.setText(""); // Limpiar campo de texto después de enviar mensaje
                }
            }
        });
        mensajePanel.add(enviarButton, BorderLayout.EAST);
        chatPanel.add(mensajePanel, BorderLayout.SOUTH);

        panel.add(chatPanel, BorderLayout.EAST);

        // Botón para salir de la ventana
        salirButton = new JButton("Salir");
        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Cerrar la ventana
            }
        });
        panel.add(salirButton, BorderLayout.SOUTH);

        getContentPane().add(panel);
    }

    private void agregarFilas(DefaultTableModel model, List<Estudiante> estudiantes, Estudiante usuarioLogeado) {
        Random random = new Random();

        // Agregar el usuario logueado primero
        model.addRow(new Object[]{usuarioLogeado.getNombre(), usuarioLogeado.getApellido(), usuarioLogeado.getEstiloAprendizaje(), usuarioLogeado.getCarrera(), usuarioLogeado.getCiclo()});
        estudiantes.remove(usuarioLogeado); // Eliminar el usuario logueado de la lista

        // Agregar hasta 4 estudiantes adicionales
        for (int i = 0; i < Math.min(estudiantes.size(), 4); i++) {
            int index = random.nextInt(estudiantes.size());
            Estudiante estudiante = estudiantes.get(index);
            model.addRow(new Object[]{estudiante.getNombre(), estudiante.getApellido(), estudiante.getEstiloAprendizaje(), estudiante.getCarrera(), estudiante.getCiclo()});
            estudiantes.remove(index); // Eliminar el estudiante seleccionado para evitar repetición
        }
    }

    public void agregarMensajeChat(String mensaje) {
        chatTextArea.append(mensaje + "\n");
    }
}
