<h1><b>Código BETA para Sistema multiagente (SMA) que permita a los estudiantes de la Universidad Nacional de Loja encontrar y conectarse con compañeros interesados en formar grupos de estudio.</b></h1>

# 📗 Tabla of Contenidos

- [🤝 Acerca del Proyecto](#titulo)
    - [🔭 Objetivo del Proyecto](#objetivos)
- [📖 Estructura del Proyecto](#estructura-proyecto)
    - [Agente Autenticacion](#agente-autenticacion)
    - [Agente Login](#agente-login)
    - [Agente Estudiante Selector](#agente-estudiante)
    - [Agente Formacion Grupos](#agente-formacion)
    - [Agente Visualizacion Estudiantes](#agente-visualizador)
- [🛠 Requisistos](#🛠-requisitos)
- [🚀 Capturas de Pantalla](#capturas)
- [💻 Instrucciones](#instrucciones)
    - [Inicio Sesión](#inicio)
    - [Registro Nuevo Estudiante](#registro)
    - [Formación Grupos Estudio](#grupo)
    - [Visualizar Estudiante por Estilo](#estilo)
    - [Agregar mensajes al grupo de estudio](#mensajes)
    - [Cerrar Sesión](#cerrar)
- [👥 Autor](#autor)


# 🤝 [SMA Grupo De Estudio] <a name="titulo"></a>
Facilita la conexión entre estudiantes con horarios y materias similares, permitiendo la formación de grupos de estudio eficientes. Esto ayuda a optimizar el tiempo de estudio al promover la colaboración entre estudiantes con intereses académicos comunes.

## 🔭 [Objetivo del Proyecto] <a name="objetivos"></a>
El objetivo del SMA es ayudar a los usuarios a optimizar su tiempo de estudio al proporcionar una herramienta eficiente para encontrar compañeros de estudio con horarios, materias similares y promover un ambiente colaborativo donde los estudiantes puedan compartir conocimientos, resolver dudas y trabajar juntos en proyectos académicos.

# 📖 [Estructura Del Proyecto] <a name="estructura-proyecto"></a>
[![Captura-de-pantalla-2024-02-09-a-la-s-18-22-19.png](https://i.postimg.cc/15gByjLZ/Captura-de-pantalla-2024-02-09-a-la-s-18-22-19.png)](https://postimg.cc/vcsfz3YP)

- ### *Agente Autenticación:* <a name="agente-autenticacion"></a> 
Se implementa un agente de autenticación en un sistema multiagente utilizando JADE, que verifica credenciales de usuario y recupera datos estudiantiles de una base de datos MySQL, respondiendo a mensajes de solicitud de inicio de sesión y solicitud de datos estudiantiles enviados por otros agentes.

- ### *Agente Login:* <a name="agente-login"></a> 
Se implementa un agente de inicio de sesión (`AgenteLogin`) que proporciona una interfaz gráfica de usuario (GUI) para que los usuarios ingresen sus credenciales. La GUI contiene campos de texto para el nombre de usuario y la contraseña, así como botones para iniciar sesión y registrarse. Cuando un usuario intenta iniciar sesión, se envía un mensaje al agente de autenticación (`AgenteAutenticacion`) con las credenciales proporcionadas. Si la autenticación es exitosa, se muestra un mensaje de confirmación y se abre una ventana que muestra los datos del estudiante correspondientes al usuario autenticado. Si la autenticación falla, se muestra un mensaje de error. Si el usuario decide registrarse, se abre una ventana de registro (`AgenteRegistro`).

- ### *AgenteEstudianteSelector:* <a name="agente-estudiante"></a> 
Este agente de selección de estudiantes (AgenteEstudianteSelector) en un sistema multiagente utilizando JADE. El agente recibe mensajes de solicitud de formación de grupos de estudio, selección de compañeros de grupo y adición de estudiantes a grupos existentes. El comportamiento del agente se define en la clase interna EstudianteSelectorBehaviour, que maneja la lógica de procesamiento de los mensajes recibidos. Cuando se solicita la formación de un grupo, el agente selecciona a los estudiantes adecuados según su estilo de aprendizaje y los envía al solicitante. Una vez que se seleccionan los compañeros de grupo, se forma un nuevo grupo de estudio o se agregan estudiantes a un grupo existente, con un límite máximo de 5 estudiantes por grupo.

- #### *Filtrado colaborativo:*
Un filtrado colaborativo es el proceso de predecir los intereses de un usuario mediante la identificación de preferencias e información de muchos usuarios. Esto se hace filtrando datos en busca de información o patrones utilizando técnicas que involucran la colaboración entre múltiples agentes, fuentes de datos, etc. La intuición detrás del filtrado colaborativo es que, si los usuarios «A» y «B» tienen gustos similares en un producto, entonces es probable que «A» y «B» tienen un gusto similar en otros productos también. 

La distancia euclidea es una medida de la distancia entre dos puntos en un espacio bidimensional o tridimensional. Se calcula utilizando la fórmula de la distancia euclidea, que es la raíz cuadrada de la suma de las diferencias entre las coordenadas de cada punto, elevadas al cuadrado.

- ### *AgenteFormacionGrupos:* <a name="agente-formacion"></a> 
Agente de formación de grupos (AgenteFormacionGrupos) en un sistema multiagente utilizando JADE. El agente recibe solicitudes para formar un grupo de estudio con un estilo de aprendizaje específico. Luego, selecciona un compañero de estudio aleatorio para el usuario que inició la solicitud y envía la información del grupo al agente solicitante. Utiliza una base de datos MySQL para obtener la información de los estudiantes y muestra los estudiantes seleccionados en una ventana de mensaje para su visualización. La formación de grupo se realiza de forma aleatoria, limitando la selección a un máximo de 5 estudiantes.

- ### *AgenteVisualizacionEstudiantes* <a name="agente-visualizador"></a> 
Es un agente que responde a mensajes de solicitud para obtener una lista de estudiantes por estilo de aprendizaje desde una base de datos MySQL. El agente utiliza un comportamiento cíclico para recibir y procesar mensajes. Cuando recibe un mensaje de solicitud que comienza con "Estudiantes por estilo:", extrae el estilo de aprendizaje del contenido del mensaje y realiza una consulta a la base de datos para obtener la lista de estudiantes con ese estilo. Luego, construye una lista de cadenas que contienen información sobre cada estudiante (nombres, apellidos, estilo de aprendizaje, carrera y ciclo), y envía esta lista como contenido de un mensaje de respuesta al remitente del mensaje original. Además, muestra la lista de estudiantes en una interfaz gráfica de usuario (VisualizacionEstudiantesGui).

# 🛠 [Requisitos] <a name="requisitos"></a>
- **[IDE]** 
Herramienta que integra funciones para el desarrollo de software, como edición de código y depuración.
- **[JAVA]**
Lenguaje de programación multiplataforma y orientado a objetos, destacado por su portabilidad.
- **[JADE]**
Framework de desarrollo de agentes en Java, utilizado para sistemas multiagente.
- **[Mysql]**
Sistema de gestión de bases de datos relacional de código abierto y ampliamente utilizado en aplicaciones web y empresariales.
- **[BaseDatos]**
La base de datos debe llevar el nombre de: sma_grupo

# 🚀 Capturas de Pantalla <a name="capturas"></a>

<div style="display: flex; flex-wrap: wrap;">
    <a href="https://postimg.cc/G4pQFqKW" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/Nj6zwnHj/Captura-de-pantalla-2024-02-09-a-la-s-18-42-12.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-42-12.png" width="400" height="auto"/>
    </a>
    <a href="https://postimg.cc/tnydY57m" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/D04xpphF/Captura-de-pantalla-2024-02-09-a-la-s-18-42-24.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-42-24.png" width="400" height="auto"/>
    </a>
    <a href="https://postimg.cc/hhqNLRdN" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/xjkYS0Tj/Captura-de-pantalla-2024-02-09-a-la-s-18-42-37.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-42-37.png" width="400" height="auto"/>
    </a>
    <a href="https://postimg.cc/nX5PMyxR" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/hGPWZn5B/Captura-de-pantalla-2024-02-09-a-la-s-18-43-29.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-43-29.png" width="400" height="auto"/>
    </a>
    <a href="https://postimg.cc/ftQqfYJY" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/k42r9cCY/Captura-de-pantalla-2024-02-09-a-la-s-18-43-51.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-43-51.png" width="400" height="auto"/>
    </a>
    <a href="https://postimg.cc/ZvTf1ZPR" style="margin-right: 10px; margin-bottom: 10px;">
        <img src="https://i.postimg.cc/fyd6SRJx/Captura-de-pantalla-2024-02-09-a-la-s-18-44-26.png" alt="Captura-de-pantalla-2024-02-09-a-la-s-18-44-26.png" width="400" height="auto"/>
    </a>
</div>



# 💻 Instrucciones <a name="instrucciones"></a>

### *Inicio de sesión:* <a name="inicio"></a>
- Abre la aplicación y selecciona la opción "Iniciar Sesión".
- Ingresa tu nombre de usuario y contraseña.
- Haz clic en el botón "Iniciar Sesión".

### *Registro de nuevo estudiante:* <a name="registro"></a>
Si eres un nuevo usuario, selecciona la opción "Registrarse".
- Completa todos los campos del formulario de registro con tus datos personales.
- Selecciona tus preferencias de aprendizaje en las preguntas desplegables.
- Haz clic en el botón "Registrar" para completar el registro.

### *Formación de grupos de estudio:* <a name="grupo"></a>
Una vez iniciada la sesión, selecciona la opción para formar un nuevo grupo de estudio.
- Selecciona el estilo de aprendizaje que deseas para tu grupo.
- Selecciona a los compañeros de grupo de la lista presentada.
- Confirma la selección y espera la confirmación de la formación del grupo.

### *Visualización de estudiantes por estilo:* <a name="estilo"></a>
Si deseas ver una lista de estudiantes con un estilo de aprendizaje específico:
- Selecciona la opción "Visualizar Estudiantes por Estilo".
- Elige el estilo de aprendizaje deseado.
- Espera la lista de estudiantes que se mostrará en la interfaz gráfica.

### *Agregar mensajes al grupo de estudio:* <a name="mensajes"></a>
Una vez que estés en un grupo de estudio, puedes agregar mensajes al chat del grupo.
Escribe tu mensaje en el campo de texto y haz clic en el botón "Enviar" para compartirlo con tus compañeros de grupo.

### *Cerrar sesión:* <a name="cerrar"></a>
Cuando hayas terminado de usar la aplicación, cierra sesión para proteger tu cuenta.
- Selecciona la opción "Cerrar Sesión" para finalizar tu sesión actual.

# 👥 Autor <a name="autor"></a>

👤 **Santiago Tene Castillo**
- GitLab: [@githubhandle](https://gitlab.com/santy10e)
